# AnimeDLR is an application that allows you to grab anime, cartoon and drama episodes on streaming sites so you can watch them online (no need to download the episode before watching it) or offline (the episode is downloaded before you can watch it) on your android device. You can keep track of the episodes watched, add series in your favorites.

## Features
* Choose the server where you want to watch or download your anime.
* Add anime to a download queue.
* Bookmark your preferred series.
* Watch your anime online or offline.
* MyAnimeList, Kitsu, AniList synchronization.

## Requirement:
* Android 4.1 or higher (API 16).

## Downloads:
* [Github](https://cylonu87.github.io/androidapps/)
* [Wix](https://cyberneticlifeform.wixsite.com/cylonu87/animedlr)
* [Bitbucket](https://bitbucket.org/cylonu87/animedlr/downloads/)